#include "stdafx.h"

void Placeholders::Create(
    _In_ QString sourcePath,
    _In_ QString sourceSubDir,
    _In_ QString destPath)
{
    CF_PLACEHOLDER_CREATE_INFO cloudEntry;

    if (sourcePath.back() != L'\\')
    {
        sourcePath.push_back(L'\\');
    }
;
    if (sourceSubDir.length() && sourceSubDir.back() != '\\')
    {
        sourceSubDir.push_back(L'\\');
    }
    QString fullDestPath = destPath + L'\\' + sourceSubDir;
    QString filePattern = sourcePath + sourceSubDir;

    QDir directory(filePattern);
    QFileInfoList fileInfoList = directory.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
    for (const QFileInfo& fileInfo : fileInfoList)
    {
        QString relativeName = sourceSubDir + fileInfo.fileName();

        cloudEntry.FileIdentity = CloudAPI::QStringToWChar(relativeName);
        cloudEntry.FileIdentityLength = (DWORD)((relativeName.size() + 1) * sizeof(WCHAR));

        cloudEntry.RelativeFileName = CloudAPI::QStringToWChar(fileInfo.fileName());
        cloudEntry.Flags = CF_PLACEHOLDER_CREATE_FLAG_MARK_IN_SYNC;
        cloudEntry.FsMetadata.FileSize.QuadPart = (ULONGLONG)fileInfo.size();
        cloudEntry.FsMetadata.BasicInfo.FileAttributes = fileInfo.isDir() ? FILE_ATTRIBUTE_DIRECTORY : FILE_ATTRIBUTE_NORMAL;
        cloudEntry.FsMetadata.BasicInfo.CreationTime = QDateTimeToLargeInteger(fileInfo.birthTime());
        cloudEntry.FsMetadata.BasicInfo.LastWriteTime = QDateTimeToLargeInteger(fileInfo.lastModified());
        cloudEntry.FsMetadata.BasicInfo.LastAccessTime = QDateTimeToLargeInteger(fileInfo.lastRead());
        cloudEntry.FsMetadata.BasicInfo.ChangeTime = QDateTimeToLargeInteger(fileInfo.lastModified());

        if (fileInfo.isDir())
        {
            cloudEntry.Flags |= CF_PLACEHOLDER_CREATE_FLAG_DISABLE_ON_DEMAND_POPULATION;
            cloudEntry.FsMetadata.FileSize.QuadPart = 0;
        }

        CfCreatePlaceholders(CloudAPI::QStringToWChar(fullDestPath), &cloudEntry, 1, CF_CREATE_FLAG_NONE, NULL);

        if (fileInfo.isDir())
        {
            Create(sourcePath, relativeName, destPath);
        }
    }
}
