#include <qqml.h>
#include "stdafx.h"
#include <QUuid>
#include <QDebug>
#include <QDir>
CloudAPI::CloudAPI(QObject *parent)
    : QObject{parent}
{

}
CF_CALLBACK_REGISTRATION CloudAPI::s_MirrorCallbackTable[] =
    {
        { CF_CALLBACK_TYPE_FETCH_DATA, CloudAPI::OnFetchData },
        { CF_CALLBACK_TYPE_CANCEL_FETCH_DATA, CloudAPI::OnCancelFetchData },
        CF_CALLBACK_REGISTRATION_END
};
void CALLBACK CloudAPI::OnFetchData(
    _In_ CONST CF_CALLBACK_INFO* callbackInfo,
    _In_ CONST CF_CALLBACK_PARAMETERS* callbackParameters)
{
    FileCopierWithProgress::CopyFromServerToClient(callbackInfo, callbackParameters, s_SyncRootPath);
}

void CALLBACK CloudAPI::OnCancelFetchData(
    _In_ CONST CF_CALLBACK_INFO* callbackInfo,
    _In_ CONST CF_CALLBACK_PARAMETERS* callbackParameters)
{
    FileCopierWithProgress::CancelCopyFromServerToClient(callbackInfo, callbackParameters);
}
CF_CONNECTION_KEY CloudAPI::ConnectionKey;
LPCWSTR CloudAPI::s_SyncRootPath =  L"D:\\dummy2";
LPCWSTR CloudAPI::s_SyncClientPath =  L"D:\\temp2";

bool CloudAPI::connectSyncRoot()
{ 
    registerSync();

    CloudProviderRegistrar::RegisterWithShell();

    HRESULT hr = CfConnectSyncRoot(s_SyncClientPath, s_MirrorCallbackTable, NULL, CF_CONNECT_FLAG_NONE, &ConnectionKey);

    if(hr == S_OK)
    {
        Placeholders::Create(QString::fromWCharArray(s_SyncRootPath), QString::fromWCharArray(L""), QString::fromWCharArray(s_SyncClientPath));
        return true;
    }
    return false;
    emit connectSyncRooted();
}

bool CloudAPI::disConnectSyncRoot()
{
    CfUpdateSyncProviderStatus(ConnectionKey, CF_PROVIDER_STATUS_DISCONNECTED);
    HRESULT hr = CfDisconnectSyncRoot(ConnectionKey);

    if(hr == S_OK)
    {
        CloudProviderRegistrar::Unregister();
        return true;
    }

    return false;
    emit disConnectSyncRooted();
}

void CloudAPI::registerSync()
{
    CF_SYNC_REGISTRATION registration = {0};
    registration.StructSize = sizeof(CF_SYNC_REGISTRATION);
    registration.ProviderName = L"YourProviderName";
    registration.ProviderVersion = L"1.0";

    registration.SyncRootIdentity = NULL;
    registration.SyncRootIdentityLength = 0;
    registration.FileIdentity = NULL;
    registration.FileIdentityLength = 0;
    QUuid guid = QUuid::createUuid();
    registration.ProviderId = guid;

    CF_SYNC_POLICIES policies = {0};
    policies.StructSize = sizeof(CF_SYNC_POLICIES);

    CF_HYDRATION_POLICY hydrationPolicy;
    hydrationPolicy.Primary = CF_HYDRATION_POLICY_FULL;
    hydrationPolicy.Modifier = CF_HYDRATION_POLICY_MODIFIER_NONE;
    policies.Hydration = hydrationPolicy;

    CF_POPULATION_POLICY policy;
    policy.Primary = CF_POPULATION_POLICY_FULL;
    policy.Modifier = CF_POPULATION_POLICY_MODIFIER_NONE;
    policies.Population = policy;
    policies.InSync = CF_INSYNC_POLICY_NONE;
    policies.HardLink = CF_HARDLINK_POLICY_ALLOWED;
    policies.PlaceholderManagement = CF_PLACEHOLDER_MANAGEMENT_POLICY_DEFAULT;

    CF_REGISTER_FLAGS registerFlags = CF_REGISTER_FLAG_NONE;

    HRESULT gg = CfRegisterSyncRoot(s_SyncClientPath, &registration, &policies, registerFlags);
}

