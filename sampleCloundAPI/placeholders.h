#include "stdafx.h"
#pragma once
class Placeholders
{
public:
    static void Create(
        _In_ QString sourcePath,
        _In_ QString sourceSubDirstr,
        _In_ QString destPath);
    static LARGE_INTEGER QDateTimeToLargeInteger(const QDateTime& dateTime) {
        qint64 secondsSinceEpoch = dateTime.toSecsSinceEpoch();
        LARGE_INTEGER largeInteger;
        largeInteger.QuadPart = secondsSinceEpoch * 10000000LL + 116444736000000000LL; // Convert seconds to 100-nanosecond intervals and offset to FILETIME epoch
        return largeInteger;
    }
};

