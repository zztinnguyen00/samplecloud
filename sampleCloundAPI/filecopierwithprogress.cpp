#include "stdafx.h"

#define CHUNKSIZE 4096
#define CHUNKDELAYMS 250

#define FIELD_SIZE( type, field ) ( sizeof( ( (type*)0 )->field ) )
#define CF_SIZE_OF_OP_PARAM( field )                                           \
    ( FIELD_OFFSET( CF_OPERATION_PARAMETERS, field ) +                         \
      FIELD_SIZE( CF_OPERATION_PARAMETERS, field ) )

struct READ_COMPLETION_CONTEXT 
{
    OVERLAPPED Overlapped;
    CF_CALLBACK_INFO CallbackInfo;
    TCHAR FullPath[MAX_PATH];
    HANDLE Handle;
    CHAR PriorityHint;
    LARGE_INTEGER StartOffset;
    LARGE_INTEGER RemainingLength;
    ULONG BufferSize;
    BYTE Buffer[1];
};

void FileCopierWithProgress::CopyFromServerToClient(
    _In_ CONST CF_CALLBACK_INFO* lpCallbackInfo,
    _In_ CONST CF_CALLBACK_PARAMETERS* lpCallbackParameters,
    _In_ LPCWSTR serverFolder)
{

    CopyFromServerToClientWorker(
        lpCallbackInfo,
        lpCallbackInfo->ProcessInfo,
        lpCallbackParameters->FetchData.RequiredFileOffset,
        lpCallbackParameters->FetchData.RequiredLength,
        lpCallbackParameters->FetchData.OptionalFileOffset,
        lpCallbackParameters->FetchData.OptionalLength,
        lpCallbackParameters->FetchData.Flags,
        lpCallbackInfo->PriorityHint,
        serverFolder);
}

void FileCopierWithProgress::CancelCopyFromServerToClient(
    _In_ CONST CF_CALLBACK_INFO* lpCallbackInfo,
    _In_ CONST CF_CALLBACK_PARAMETERS* lpCallbackParameters)
{
    CancelCopyFromServerToClientWorker(lpCallbackInfo,
        lpCallbackParameters->Cancel.FetchData.FileOffset,
        lpCallbackParameters->Cancel.FetchData.Length,
        lpCallbackParameters->Cancel.Flags);
}

void FileCopierWithProgress::TransferData(
    _In_ CF_CONNECTION_KEY connectionKey,
    _In_ LARGE_INTEGER transferKey,
    _In_reads_bytes_opt_(length.QuadPart) LPCVOID transferData,
    _In_ LARGE_INTEGER startingOffset,
    _In_ LARGE_INTEGER length,
    _In_ NTSTATUS completionStatus)
{
    CF_OPERATION_INFO opInfo = { 0 };
    CF_OPERATION_PARAMETERS opParams = { 0 };

    opInfo.StructSize = sizeof(opInfo);
    opInfo.Type = CF_OPERATION_TYPE_TRANSFER_DATA;
    opInfo.ConnectionKey = connectionKey;
    opInfo.TransferKey = transferKey;
    opParams.ParamSize = CF_SIZE_OF_OP_PARAM(TransferData);
    opParams.TransferData.CompletionStatus = completionStatus;
    opParams.TransferData.Buffer = transferData;
    opParams.TransferData.Offset = startingOffset;
    opParams.TransferData.Length = length;

   CfExecute(&opInfo, &opParams);
}

void WINAPI FileCopierWithProgress::OverlappedCompletionRoutine(
    _In_ DWORD errorCode,
    _In_ DWORD numberOfBytesTransfered,
    _Inout_ LPOVERLAPPED overlapped)
{

    READ_COMPLETION_CONTEXT* readContext = CONTAINING_RECORD(overlapped, READ_COMPLETION_CONTEXT, Overlapped);

    auto keepProcessing{ false };
    do
    {
        if (errorCode == 0)
        {
            if (!GetOverlappedResult(readContext->Handle, overlapped, &numberOfBytesTransfered, TRUE))
            {
                errorCode = GetLastError();
            }
        }

        if (errorCode != 0)
        {
            numberOfBytesTransfered = (ULONG)readContext->BufferSize;
        }

        LARGE_INTEGER numberOfBytesTransfered_LargeInteger;
        numberOfBytesTransfered_LargeInteger.QuadPart = numberOfBytesTransfered;

        TransferData(
            readContext->CallbackInfo.ConnectionKey,
            readContext->CallbackInfo.TransferKey,
            errorCode == 0 ? readContext->Buffer : NULL,
            readContext->StartOffset,
            numberOfBytesTransfered_LargeInteger,
            errorCode);

        readContext->StartOffset.QuadPart += numberOfBytesTransfered;
        readContext->RemainingLength.QuadPart -= numberOfBytesTransfered;

        if (readContext->RemainingLength.QuadPart > 0)
        {
            DWORD bytesToRead = (DWORD)readContext->BufferSize;

            readContext->Overlapped.Offset = readContext->StartOffset.LowPart;
            readContext->Overlapped.OffsetHigh = readContext->StartOffset.HighPart;

            if (!ReadFileEx(
                readContext->Handle,
                readContext->Buffer,
                bytesToRead,
                &readContext->Overlapped,
                OverlappedCompletionRoutine))
            {
                errorCode = GetLastError();
                numberOfBytesTransfered = 0;
                keepProcessing = true;
            }
        }
        else
        {
            CloseHandle(readContext->Handle);
            HeapFree(GetProcessHeap(), 0, readContext);
        }
    }
    while (keepProcessing);
}

void FileCopierWithProgress::CopyFromServerToClientWorker(
    _In_ CONST CF_CALLBACK_INFO* callbackInfo,
    _In_opt_ CONST CF_PROCESS_INFO* processInfo,
    _In_ LARGE_INTEGER requiredFileOffset,
    _In_ LARGE_INTEGER requiredLength,
    _In_ LARGE_INTEGER,
    _In_ LARGE_INTEGER,
    _In_ CF_CALLBACK_FETCH_DATA_FLAGS,
    _In_ UCHAR priorityHint,
    _In_ LPCWSTR serverFolder)
{
    HANDLE serverFileHandle;

    QString fullServerPath = QString::fromWCharArray(serverFolder) + "/" + QString::fromWCharArray(reinterpret_cast<wchar_t const*>(callbackInfo->FileIdentity));

    QString fullClientPath = QString::fromWCharArray(callbackInfo->VolumeDosName);
    if(callbackInfo->NormalizedPath != nullptr)
        fullClientPath = fullClientPath + QString::fromWCharArray(callbackInfo->NormalizedPath);

    READ_COMPLETION_CONTEXT* readCompletionContext;
    DWORD chunkBufferSize;

    serverFileHandle = 
        CreateFile(
            CloudAPI::QStringToWChar(fullServerPath),
            GENERIC_READ,
            FILE_SHARE_READ | FILE_SHARE_DELETE,
            NULL,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
            NULL);

    if (serverFileHandle == INVALID_HANDLE_VALUE) 
    {
        qDebug() << "CopyFromServerToClientWorker: CreateFile fail";
        return;
    }

    chunkBufferSize = (ULONG)requiredLength.QuadPart;

    readCompletionContext = (READ_COMPLETION_CONTEXT*)
        HeapAlloc(
            GetProcessHeap(),
            0,
            chunkBufferSize + FIELD_OFFSET(READ_COMPLETION_CONTEXT, Buffer));

    if (readCompletionContext == NULL) 
    {
        CloseHandle(serverFileHandle);
    }

    wcsncpy_s(
        readCompletionContext->FullPath,
        CloudAPI::QStringToWChar(fullClientPath),
        wcslen(CloudAPI::QStringToWChar(fullClientPath)));

    readCompletionContext->CallbackInfo = *callbackInfo;
    readCompletionContext->Handle = serverFileHandle;
    readCompletionContext->PriorityHint = priorityHint;
    readCompletionContext->Overlapped.Offset = requiredFileOffset.LowPart;
    readCompletionContext->Overlapped.OffsetHigh = requiredFileOffset.HighPart;
    readCompletionContext->StartOffset = requiredFileOffset;
    readCompletionContext->RemainingLength = requiredLength;
    readCompletionContext->BufferSize = chunkBufferSize;

    if (!ReadFileEx(
        serverFileHandle,
        readCompletionContext->Buffer,
        chunkBufferSize,
        &readCompletionContext->Overlapped,
        OverlappedCompletionRoutine)) 
    {
        CloseHandle(serverFileHandle);
        HeapFree(GetProcessHeap(), 0, readCompletionContext);
    }
}

void FileCopierWithProgress::CancelCopyFromServerToClientWorker(
    _In_ CONST CF_CALLBACK_INFO* lpCallbackInfo,
    _In_ LARGE_INTEGER liCancelFileOffset,
    _In_ LARGE_INTEGER liCancelLength,
    _In_ CF_CALLBACK_CANCEL_FLAGS)
{

}

