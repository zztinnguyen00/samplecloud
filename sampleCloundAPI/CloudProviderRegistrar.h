#include "stdafx.h"
#pragma once
class CloudProviderRegistrar
{
public:
    static void RegisterWithShell();
    static void Unregister();

private:
    static std::unique_ptr<TOKEN_USER> GetTokenInformation();
    static QString GetSyncRootId();
    static void AddCustomState(
        _In_ winrt::IVector<winrt::StorageProviderItemPropertyDefinition>& customStates,
        _In_ LPCWSTR displayNameResource,
        _In_ int id);
};

