#include "stdafx.h"

void CloudProviderRegistrar::RegisterWithShell()
{
        auto syncRootID = GetSyncRootId();

        winrt::StorageProviderSyncRootInfo info;
        info.Id(syncRootID.toStdWString());

        auto folder = winrt::StorageFolder::GetFolderFromPathAsync(CloudAPI::s_SyncClientPath).get();
        info.Path(folder);

        info.DisplayNameResource(L"CloudMirror");

        info.IconResource(L"%SystemRoot%\\system32\\charmap.exe,0");
        info.HydrationPolicy(winrt::StorageProviderHydrationPolicy::Full);
        info.HydrationPolicyModifier(winrt::StorageProviderHydrationPolicyModifier::None);
        info.PopulationPolicy(winrt::StorageProviderPopulationPolicy::AlwaysFull);
        info.InSyncPolicy(winrt::StorageProviderInSyncPolicy::FileCreationTime | winrt::StorageProviderInSyncPolicy::DirectoryCreationTime);
        info.Version(L"1.0.0");
        info.ShowSiblingsAsGroup(false);
        info.HardlinkPolicy(winrt::StorageProviderHardlinkPolicy::None);

        winrt::Uri uri(L"http://cloudmirror.example.com/recyclebin");
        info.RecycleBinUri(uri);

        QString syncRootIdentity = QString::fromWCharArray(CloudAPI::s_SyncRootPath) + "->" + QString::fromWCharArray(CloudAPI::s_SyncClientPath);

        winrt::IBuffer contextBuffer = winrt::CryptographicBuffer::ConvertStringToBinary(syncRootIdentity.toStdWString(), winrt::BinaryStringEncoding::Utf8);
        info.Context(contextBuffer);

        winrt::IVector<winrt::StorageProviderItemPropertyDefinition> customStates = info.StorageProviderItemPropertyDefinitions();
        AddCustomState(customStates, L"CustomStateName1", 1);
        AddCustomState(customStates, L"CustomStateName2", 2);
        AddCustomState(customStates, L"CustomStateName3", 3);

        winrt::StorageProviderSyncRootManager::Register(info);
}

void CloudProviderRegistrar::Unregister()
{
    winrt::StorageProviderSyncRootManager::Unregister(GetSyncRootId().toStdWString());
}

std::unique_ptr<TOKEN_USER> CloudProviderRegistrar::GetTokenInformation()
{
    std::unique_ptr<TOKEN_USER> tokenInfo;

    auto tokenHandle{ GetCurrentThreadEffectiveToken() };

    DWORD tokenInfoSize{ 0 };
    if (!::GetTokenInformation(tokenHandle, TokenUser, nullptr, 0, &tokenInfoSize))
    {
        if (::GetLastError() == ERROR_INSUFFICIENT_BUFFER)
        {
            tokenInfo.reset(reinterpret_cast<TOKEN_USER*>(new char[tokenInfoSize]));
            if (!::GetTokenInformation(tokenHandle, TokenUser, tokenInfo.get(), tokenInfoSize, &tokenInfoSize))
            {
                throw std::exception("GetTokenInformation failed");
            }
        }
        else
        {
            throw std::exception("GetTokenInformation failed");
        }
    }
    return tokenInfo;
}

QString CloudProviderRegistrar::GetSyncRootId()
{
    std::unique_ptr<TOKEN_USER> tokenInfo(GetTokenInformation());
    auto sidString = [&]() {
        winrt::com_array<wchar_t> string;
        if (::ConvertSidToStringSid(tokenInfo->User.Sid, winrt::put_abi(string)))
        {
            return string;
        }
        else
        {
            throw std::bad_alloc();
        }
    }();

    QString syncRootID =  "TestStorageProvider!" + QString::fromWCharArray(sidString.data()) + "!TestAccount1" ;
    return syncRootID;
}

void CloudProviderRegistrar::AddCustomState(
    _In_ winrt::IVector<winrt::StorageProviderItemPropertyDefinition>& customStates,
    _In_ LPCWSTR displayNameResource,
    _In_ int id)
{
    winrt::StorageProviderItemPropertyDefinition customState;
    customState.DisplayNameResource(displayNameResource);
    customState.Id(id);
    customStates.Append(customState);
}

