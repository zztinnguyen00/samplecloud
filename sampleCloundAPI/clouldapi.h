#include <QObject>
#include <QQmlEngine>
#include <qqml.h>
#include <Windows.h>
#include <cfapi.h>
class CloudAPI : public QObject
{
    Q_OBJECT

    QML_ELEMENT

public:
    explicit CloudAPI(QObject *parent = nullptr);

    Q_INVOKABLE bool connectSyncRoot();
    Q_INVOKABLE bool disConnectSyncRoot();
    static CF_CALLBACK_REGISTRATION s_MirrorCallbackTable[];
    static void CALLBACK OnFetchData(
        _In_ CONST CF_CALLBACK_INFO* callbackInfo,
        _In_ CONST CF_CALLBACK_PARAMETERS* callbackParameters);

    static void CALLBACK OnCancelFetchData(
        _In_ CONST CF_CALLBACK_INFO* callbackInfo,
        _In_ CONST CF_CALLBACK_PARAMETERS* callbackParameters);
    static CF_CONNECTION_KEY ConnectionKey;
    void registerSync();
    LPCWSTR SyncRootPath;
    LPCWSTR SyncClientPath;
    static LPCWSTR s_SyncRootPath;
    static LPCWSTR s_SyncClientPath;
    static wchar_t* QStringToWChar(const QString& qstr)
    {
        const wchar_t* wcharString = reinterpret_cast<const wchar_t*>(qstr.utf16());

        size_t length = wcslen(wcharString) + 1;
        wchar_t* result = new wchar_t[length];
        wmemcpy(result, wcharString, length);

        return result;
    }
signals:
    bool connectSyncRooted();
    bool disConnectSyncRooted();
    bool listFilesInRooted();
};
