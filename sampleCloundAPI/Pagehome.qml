import QtQuick
import QtQuick.Controls
import QtQuick.Layouts


// Include các khai báo hàm của Cloud Filter API
// Đảm bảo rằng bạn đã thêm tệp cfapi.h vào dự án của mình

Rectangle {
    id: control
    anchors.fill: parent

    ColumnLayout{
        width: parent.width

        RowLayout{
            Layout.fillWidth: true
            Layout.preferredHeight: 40
            spacing: 10

            Button {
                id: connectButton
                text: "DisConnect Sync Root"

                onClicked: {
                    var result = cloudAPI.disConnectSyncRoot()
                    if (result == true) {
                        console.log("DisConnected sync root Done")
                    } else {
                        console.error("Failed to Dis connect sync root")
                    }
                }
            }
        }

        Rectangle{
            Layout.fillWidth: true
            Layout.preferredHeight: control.height - 40
            color: "blue"

        }

    }
}
