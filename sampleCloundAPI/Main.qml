import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import "."
ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    CloudAPI{
        id: cloudAPI
    }

    Rectangle {
        id: _connect
        anchors.fill: parent

        Button {
                id: connectButton
                text: "Connect to Sync Root"
                anchors.centerIn: parent

                onClicked: {
                    // Gọi hàm kết nối đến sync root
                    var syncRootPath = "/path/to/your/sync/root"
                    var result = cloudAPI.connectSyncRoot()

                    // Kiểm tra kết quả và hiển thị thông báo tương ứng
                    if (result == true) {
                        console.log("Connected to sync root at: " + syncRootPath)
                        _home.visible = true;
                        _connect.visible = false;
                    } else {
                        console.error("Failed to connect to sync root")
                    }
                }
            }
    }
    Component.onCompleted: {

    }
    Pagehome {
        id: _home
        visible: false
    }
}
